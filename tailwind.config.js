module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
    defaultLineHeights: true,
    standardFontWeights: true,
  },
  purge: ["./src/**/*.js", "./src/**/*.html"],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
};
