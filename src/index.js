import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    const className = 'square h-12 w-12 text-3xl bg-indigo-100 border border-indigo-800 sm:w-20 sm:h-20 font-mono font-bold sm:text-5xl text-indigo-800' + (props.highlight ? ' highlight' : '');
    return (
        <button
            className={className}
            onClick={props.onClick}
        >
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare(i) {
        const winnerLine = this.props.winnerLine;
        return (
            <Square
                key = {i}
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
                highlight = {winnerLine && winnerLine.includes(i)}
            />
        );
    }

    render() {
        const boardSize = 3;
        let squares = [];
        for (let i = 0; i < boardSize; i++) {
            let row = [];
            for (let j = 0; j < boardSize; j++) {
                row.push(this.renderSquare(i * boardSize + j));
            }
            squares.push(<div key={i} className="board-row">{row}</div>);
        }
        return <div>{squares}</div>;
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [
                {
                    squares: Array(9).fill(null),
                }
            ],
            stepNumber: 0,
            xIsNext: true,
            isAscending: true,
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares).winner || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
                latestMoveSquare: i,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
        });
    }

    handleSortToggle() {
        this.setState({
            isAscending: !this.state.isAscending,
        })
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        })
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winnerInfo = calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const className = 'bg-indigo-200 text-indigo-800 rounded p-1 ' + (move === this.state.stepNumber ? 'font-bold' : '');
            const latestMoveSquare = step.latestMoveSquare;
            const col = 1 + latestMoveSquare % 3;
            const row = 1 + Math.floor(latestMoveSquare / 3);
            const desc = move ?
                'Tour n°' + move + " (" + row + "," + col + ")" :
                'Début de la partie';
            return (
                <li key={move}>
                    <button className={className} onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let status;
        if (winnerInfo.winner) {
            status = winnerInfo.winner + ' a gagné';
        } else if(winnerInfo.boardIsFull) {
            status = 'Match nul !'
        } else {
            status = 'Prochain joueur : ' + (this.state.xIsNext ? 'X' : 'O');
        }

        if(!this.state.isAscending) {
            moves.reverse();
        }

        return (
            <div>
                <header className="text-center w-screen bg-indigo-200 p-4">
                    <div className="font-bold font-display text-3xl text-indigo-800 uppercase">
                        Tic tac toe
                    </div>
                    <div className="font-display text-xl text-indigo-500">
                        Le véritable jeu du Morpion !
                    </div>
                </header>
                <div className="container max-w-2xl mx-auto space-y-8 px-2">
                    <div className="text-xl mt-8 text-center sm:text-left font-semibold text-indigo-800">{status}</div>
                    <div className="space-y-8 flex flex-col sm:flex-row items-start sm:space-y-0 sm:space-x-16">
                        <div className="mx-auto sm:mx-0">
                            <Board
                                squares={current.squares}
                                onClick={(i) => this.handleClick(i)}
                                winnerLine={winnerInfo.line}
                            />
                        </div>
                        <div className="mx-auto sm:mx-0 space-y-4 bg-indigo-100 p-4 rounded">
                            <div className="text-indigo-800 font-bold">Historique des mouvements</div>
                            <button 
                                className="flex transition-colors duration-300 ease-in-out opacity-50 hover:opacity-100 focus:opacity-100 items-center text-indigo-700 px-3 py-1 border border-indigo-300 font-medium rounded"
                                onClick={() => this.handleSortToggle()}
                                >
                                <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" className="w-5 h-5 mr-1 fill-current">
                                    <g className="">
                                        <path d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M3 17v2h6v-2H3zM3 5v2h10V5H3zm10 16v-2h8v-2h-8v-2h-2v6h2zM7 9v2H3v2h4v2h2V9H7zm14 4v-2H11v2h10zm-6-4h2V7h4V5h-4V3h-2v6z"></path>
                                    </g>
                                </svg>
                                <span className="text-sm">
                                    {this.state.isAscending ? 'Afficher le dernier coup en premier':'Afficher le dernier coup en dernier'}
                                </span>
                            </button>
                            <ol className="space-y-2">{moves}</ol>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return {
                winner: squares[a],
                line: lines[i],
                boardIsFull: false,
            };
        }
    }

    let boardIsFull = true;
    for (let i = 0; i < lines.length; i++) {
        if (squares[i] === null) {
            boardIsFull = false;
            break;
        }
    }

    return {
        winner: null,
        line: null,
        boardIsFull: boardIsFull,
    };
}

// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root')
);
